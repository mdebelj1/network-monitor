<?php
include 'connect.php';

$sql = "SELECT * FROM network_monitor";    
$result = $con->query($sql);
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="theme-color" content="#ecc70a">
		
        <title>Offline Sync</title>
  
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="./css/online-status.css" media="all" />
        <link rel="stylesheet" href="./css/style.css">
    </head>
    <body>

        <!-- Offline Snackbar -->
        <div id="online-status"></div>
        <div id="overlay"></div>
 
        <div class="content">
            <h1>Time spent Offline</h1>

            <!-- Reload page button -->
            <button id="btn_reload" onClick="window.location.reload()">RELOAD PAGE</button>

            <!-- Time spent offline -->
            <?php 
                    echo "<table style='border:3px;'>";
                    echo "<tr>";
                        echo "<th>ID</th>";
                        echo "<th>OFFLINE START</th>";
                        echo "<th>OFFLINE END</th>";
                        echo "<th>TIME DIFF</th>";
                        echo "<th>DELETE</th>";
                    echo "</tr>";
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                        echo "<tr>";
                            echo "<td>". $row["id"] ."</td>";
                            echo "<td>". $row["online_end_time"] ."</td>";
                            echo "<td>". $row["online_start_time"] . "</td>";
                            echo "<td>". $row["time_diff"] ." s</td>";
                            echo "<td><a href=\"delete.php?id=".$row['id']."\">Delete</a></td>";
                        echo "</tr";
                    echo "</table>";
                }
            }
            ?>
        </div>
        
        <!-- Scripts -->
        <script src="./js/online-status.js"></script>
        <script src="./js/network-monitor.js"></script>

    </body>
</html>