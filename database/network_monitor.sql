-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2019 at 02:59 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sync`
--

-- --------------------------------------------------------

--
-- Table structure for table `network_monitor`
--

CREATE TABLE `network_monitor` (
  `id` int(11) NOT NULL,
  `online_start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `online_end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_diff` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `network_monitor`
--

INSERT INTO `network_monitor` (`id`, `online_start_time`, `online_end_time`, `time_diff`) VALUES
(22, '2019-01-11 13:54:02', '2019-01-11 13:52:52', '70'),
(23, '2019-01-11 13:56:31', '2019-01-11 13:56:28', '3');

-- --------------------------------------------------------

--
-- Indexes for dumped tables
--

--
-- Indexes for table `network_monitor`
--
ALTER TABLE `network_monitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `network_monitor`
--
ALTER TABLE `network_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
