// ONLINE/OFFLINE provjera
document.addEventListener('DOMContentLoaded', function() {
    var status = document.getElementById("online-status");
    var overlay = document.getElementById("overlay");
  
    // Promjena boje navbara u crveno
    function changeThemeColorOffline() {
      var metaThemeColor = document.querySelector("meta[name=theme-color]");
      metaThemeColor.setAttribute("content", "#f67a6e");
    }
    
    // Promjena boje navbara u zeleno
    function changeThemeColorOnline() {
      var metaThemeColor = document.querySelector("meta[name=theme-color]");
      metaThemeColor.setAttribute("content", "#acd206");
    }
    
    // Promjena boje navbara u sivo
    function changeThemeColorConnected() {
      var metaThemeColor = document.querySelector("meta[name=theme-color]");
      metaThemeColor.setAttribute("content", "#5b6e84");
    }
    
    // Provjera ima li browser pristup mreži
    if (navigator.onLine) {
      // console.log('online');
      console.count("Online");
      console.timeEnd('Offline time');
      console.time('Online time');
      // $(".btn-success").prop("disabled", false);
      // $(":button").prop("disabled", false);
      overlay.style.display = "none";
      changeThemeColorOnline();
      setTimeout(function() {
        changeThemeColorConnected();
      }, 1500);
    } else {
      // console.log('offline');
      console.count("Offline");
      console.timeEnd('Online time');
      console.time('Offline time');
      status.className = "offline";
      status.style.display = "block";
      status.style.height = "18px";
      status.innerHTML = "Network error".toUpperCase();
      // $(".btn-success").prop("disabled", true);
      // $(":button").prop("disabled", true);
      overlay.style.display = "block";
      changeThemeColorOffline();
    }
  
    // Provjera ima li promjene u stanju mreže
    function updateStatus(event) {
      var condition = navigator.onLine ? "online" : "offline";
  
      status.className = condition;
      if(condition === "online") {
        status.innerHTML = "Back online".toUpperCase();
        console.count("Online");
        console.timeEnd('Offline time');
        console.time('Online time');
      } else {
        status.innerHTML = "Network error".toUpperCase();
        console.count("Offline");
        console.timeEnd('Online time');
        console.time('Offline time');
      }
      // console.log("Event: " + event.type + "; Status: " + condition);
  
      // Offline snackbar ostaje do ponovnog uspostavljanja veze
      if(status.className === "online"){        
        setTimeout(function() { 
          status.className = status.className.replace(condition, "");
          status.style.display = "block";
          status.innerHTML = "";
          overlay.style.display = "none";
          changeThemeColorConnected();
        }, 3000);
        // $(".btn-success").prop("disabled", false);
        // $(":button").prop("disabled", false);
        changeThemeColorOnline();
      } else {
        status.style.display = "block";
        // $(".btn-success").prop("disabled", true);
        // $(":button").prop("disabled", true);
        overlay.style.display = "block";
        changeThemeColorOffline();
      }
    }
    
    window.addEventListener('online',  updateStatus);
    window.addEventListener('offline', updateStatus);
  });