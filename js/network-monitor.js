// timestamp function
function NOW() {
    var date = new Date();
    var aaaa = date.getUTCFullYear();
    var gg = date.getUTCDate();
    var mm = (date.getUTCMonth() + 1);
  
    if (gg < 10)
        gg = "0" + gg;
  
    if (mm < 10)
        mm = "0" + mm;
  
    var cur_day = aaaa + "-" + mm + "-" + gg;
  
    var hours = date.getHours()
    var minutes = date.getUTCMinutes()
    var seconds = date.getUTCSeconds();
  
    if (hours < 10)
        hours = "0" + hours;
  
    if (minutes < 10)
        minutes = "0" + minutes;
  
    if (seconds < 10)
        seconds = "0" + seconds;
  
    return cur_day + " " + hours + ":" + minutes + ":" + seconds;
  }

// check when it went offline
var wentOffline, wentOnline;
function handleConnectionChange(event){
    if(event.type == "offline"){
        console.log("You lost connection.");
        wentOffline = NOW();
    }
    if(event.type == "online"){
        console.log("You are now back online.");
        wentOnline = NOW();
        // console.log("You were offline for " + (wentOnline - wentOffline) + " seconds.");
    }
    fetch('./network-monitor.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify({
            online_start_time: wentOnline,
            online_end_time: wentOffline
        })
    })
}
window.addEventListener('online', handleConnectionChange);
window.addEventListener('offline', handleConnectionChange);